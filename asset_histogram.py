'''
wilson plugin to create histograms
from riskiq workspace asset data
'''
import json
from common import maxlen, padding

def makehistogram(dataset, bucket_size):
    '''
    make a histogram using the data and size
    '''
    data = json.load(open('assets.json'))
    #generate a list of tuples (workspace name, number of assets)
    #for use in making the histogram
    dataset = [(data[workspace]['name'], data[workspace][dataset])
               for workspace in data if dataset in data[workspace]]

    spacing = maxlen(dataset)
    result = [i[0]+ padding(spacing, i[0]) +\
             #workspace + enough spaces to make the histogram line up
             #to make it pretty and easy to read
              -(-i[1]/bucket_size)*"#" #upside down floor division
              for i in sorted(dataset, key=lambda x: x[1])]
              #also sort it for easier spotting of outliers
    return '\n'.join(result)

