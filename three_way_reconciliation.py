'''
below are the generated fields for risk reporting

hosts                                           -- DONE
blacklistedHosts                        -- cannot reconcile
blacklistedHostsPhish           -- cannot reconcile
blacklistedHostsMalware         -- cannot reconcile
ownedAsns                                       -- DONE
thirdPartyAsns
websites                                        -- DONE
uniqueRegistrars
uniqueRegistrants
domains                                         -- DONE
sslCerts                                        -- DONE
sslCertOrgs                                     -- cannot 3way rec
sslCertOrgUnits                         -- cannot 3way rec
sslCertExpired
sslCertSha1
sslCertMd5
'''

import json
from elasticsearch import Elasticsearch
from collections import defaultdict
from common import querydb as query_db
es = Elasticsearch(['http://es01.riskiq:9200/'])


# generalized sql query for asset counts based on type
get_db_assets_by_type_and_workspace = '''SELECT commonasset.*
FROM commonasset
JOIN workspaces using(workspaceid)
WHERE assetType=%s
AND inventorystate= 'B'
AND workspaces.paused = 0
AND workspaces.test = 0
AND workspaces.demo = 0
AND workspaces.statusCode='Y'
AND commonasset.statusCode='Y'
AND workspaceid=%s;'''

get_db_registrar_count_by_workspace = '''SELECT count( distinct(domainasset.registrarID))
FROM domainasset
JOIN commonasset using (assetid)
JOIN workspaces using(workspaceid)
WHERE commonasset.assetType='F'
AND commonasset.inventorystate= 'B'
AND workspaces.paused = 0
AND workspaces.test = 0
AND workspaces.demo = 0
AND workspaces.statusCode='Y'
AND commonasset.statusCode='Y'
AND workspaceid=%s;'''

get_db_registrant_count_by_workspace = '''SELECT count( distinct(domainasset.registrantPOCID))
FROM domainasset
JOIN commonasset using (assetid)
JOIN workspaces using(workspaceid)
WHERE commonasset.assetType='F'
AND commonasset.inventorystate= 'B'
AND workspaces.paused = 0
AND workspaces.test = 0
AND workspaces.demo = 0
AND workspaces.statusCode='Y'
AND commonasset.statusCode='Y'
AND workspaceid=%s;'''



get_dw_metrics_by_workspace = '''SELECT * FROM riskMetricsFact
WHERE riskMetricRunID = (SELECT riskMetricRunID FROM riskMetricRun WHERE completed = 1 ORDER BY updatedAt DESC LIMIT 1)
AND workspaceID in ( '%s' )
AND workspaceTagID = 0
AND workspaceBrandID = 0
AND workspaceOrganizationID = 0;'''


es_query_confirmed_assets = {"query" : {"match" : {"inventoryState" : "CONFIRMED"}}}


def get_db_assets(workspaceid):
    db_assets = {}
    db_assets['Hosts'] = query_db(get_db_assets_by_type_and_workspace, ('A', workspaceid))
    db_assets['ASNs'] = query_db(get_db_assets_by_type_and_workspace, ('E', workspaceid))
    db_assets['Websites'] = query_db(get_db_assets_by_type_and_workspace, ('C', workspaceid))
    db_assets['Domains'] = query_db(get_db_assets_by_type_and_workspace, ('F', workspaceid))
    db_assets['SSLCerts'] = query_db(get_db_assets_by_type_and_workspace, ('G', workspaceid))

    db_assets['Registrars'] = query_db(get_db_registrar_count_by_workspace, (workspaceid))
    db_assets['Registrants'] = query_db(get_db_registrant_count_by_workspace, (workspaceid))

    return db_assets


def get_dw_metrics(workspaceID):
    dw_metrics = {}
    dw_metrics = query_db(get_dw_metrics_by_workspace, workspaceID)

    return dw_metrics


def add_entry(dictionary, key, value):
    '''
    add value to dictionary[key] if key exists
    otherwise, initialize the key
    '''
    if key in dictionary:
        dictionary[key] |= {value}
    else:
        dictionary[key] = {value}
    return dictionary


def get_es_assets(workspaceid):
    es_response = query_es(workspaceid, es_query_confirmed_assets)
    #query for all assets in a single query
    es_assets = {
        'Hosts': es_response['HostAsset2'],
        'ASNs': es_response['AsnAsset'],
        'Websites': es_response['WebSiteAsset'],
        'Domains': es_response['DomainAsset'],
        'SSLCerts': es_response['SslCertAsset'],
    }

    return es_assets


def query_es(workspaceid, query):
    assets = {}
    new_assets = True
    page = es.search(
        index='asset-%s' % workspaceid,
        scroll='2m',
        search_type='scan',
        size=10000,
        body=query,
        fields = [] # don't return unused fields
        )
    while new_assets:
        new_assets = False
        page = es.scroll(scroll_id=page['_scroll_id'], scroll='2m')
        for asset in page['hits']['hits']:
            new_assets = True;
            assets = add_entry(assets, asset['_type'], asset['_id'])
    return assets


# Perform the three way reconcilation between Mysql, The Fact table, and Elastic Search
def compare_datastores(workspaceid, db_results, dw_results, es_results):
    #hosts
    db_hostcnt = len(db_results['Hosts'])
    dw_hostcnt = dw_results[0][5];
    es_hostcnt = len(es_results['Hosts'])

    #asns
    db_asncnt = len(db_results['ASNs'])
    dw_asncnt = dw_results[0][10]
    es_asncnt = len(es_results['ASNs'])

    #websites
    db_websitecnt = len(db_results['Websites'])
    dw_websitecnt = dw_results[0][12]
    es_websitecnt = len(es_results['Websites'])

    #unique regisrars
    db_registrarcnt = db_results['Registrars'][0]
    dw_registrarcnt = dw_results[0][13]
    es_registrarcnt = 'TBD'

    #unique registrants
    db_registrantcnt = db_results['Registrants'][0]
    dw_registrantcnt = dw_results[0][14]
    es_registrantcnt = 'TBD'

    #domains
    db_domaincnt = len(db_results['Domains'])
    dw_domaincnt = dw_results[0][15]
    es_domaincnt = len(es_results['Domains'])

    #sslcerts
    db_sslcertcnt = len(db_results['SSLCerts'])
    dw_sslcertcnt = dw_results[0][16]
    es_sslcertcnt = len(es_results['SSLCerts'])
    #sslCertOrgs
    #sslCertOrgUnits
    #sslCertExpired
    #sslCertSha
    #sslCertMd5


    print("WorkspaceId, Metric, MySql, Fact Table, ElasticSearch" )
    print("{0},{1},{2},{3},{4}".format(workspaceid, "Host Count", db_hostcnt, dw_hostcnt, es_hostcnt))
    print("{0},{1},{2},{3},{4}".format(workspaceid, "ASN Count", db_asncnt, dw_asncnt, es_asncnt))
    print("{0},{1},{2},{3},{4}".format(workspaceid, "Website Count", db_websitecnt, dw_websitecnt, es_websitecnt))
    print("{0},{1},{2},{3},{4}".format(workspaceid, "Registrar Count", db_registrarcnt, dw_registrarcnt, es_registrarcnt))
    print("{0},{1},{2},{3},{4}".format(workspaceid, "Registrant Count", db_registrantcnt, dw_registrantcnt, es_registrantcnt))
    print("{0},{1},{2},{3},{4}".format(workspaceid, "Domain Count", db_domaincnt, dw_domaincnt, es_domaincnt))
    print("{0},{1},{2},{3},{4}".format(workspaceid, "SSLCert Count", db_sslcertcnt, dw_sslcertcnt, es_sslcertcnt))

    return

for workspaceid in [18, 81, 275, 373, 1356]:
    # Get the raw asset data from the db
    db_results = get_db_assets(workspaceid)
    # Get the aggregates from the fact table
    dw_results = get_dw_metrics(workspaceid)
    # Get the raw asset data from ES
    es_results = get_es_assets(workspaceid)
    # Build the comparison and print.
    compare_datastores(workspaceid, db_results, dw_results, es_results)
