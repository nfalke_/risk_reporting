'''
script to update the local database
with information from the riskiq database
'''
import json
from common import querydb

def updatedb():
    '''
    updates the internal database with each asset type and also names
    '''
    getassets = '''select workspaceid, name, assetType, count(*)
    from commonasset
    join workspaces using(workspaceid)
    where inventorystate= 'B'
    and workspaces.paused = 0
    and workspaces.test = 0
    and workspaces.`demo` = 0
    and workspaces.statusCode='Y'
    and commonasset.statusCode='Y'
    group by workspaceid, assetType;
    '''
    assets = {}
    for workspaceid, workspacename, asset_type, value in querydb(getassets):
        if workspaceid in assets:
            assets[workspaceid][asset_type] = value
        else:
            assets[workspaceid] = {
                asset_type: value,
                'name': workspacename
            }
    json.dump(assets, open('assets.json', 'w'))

updatedb()
