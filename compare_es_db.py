import csv
from elasticsearch import Elasticsearch
from common import querydb, mapping, human_mapping

def writetocsv(filename, rows):
    with open(filename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for row in rows:
            writer.writerow(row)


def add_entry(dictionary, key, value):
    '''
    add value to dictionary[key] if key exists
    otherwise, initialize the key
    '''
    if key in dictionary:
        dictionary[key] |= {value}
    else:
        dictionary[key] = {value}
    return dictionary


grainmapping = {}
def get_db_assets(workspace):
    '''
    get workspace specific assets from the database
    '''
    assets = {}
    base = 'SELECT assetID, assetType, grain FROM commonAsset WHERE workspaceID = %s'
    db_query = querydb(base, workspace)

    for asset_id, asset_type, grain in db_query:
        asset_id = str(asset_id)
        grainmapping[asset_id] = grain
        assets = add_entry(assets, asset_type, asset_id)
    return assets


def get_es_assets(workspace):
    '''
    get workspace specific assets from the elastic search
    '''
    assets = {}
    es = Elasticsearch(['http://es01.riskiq:9200/'])
    page = es.search(
        index='asset-%s' % workspace,
        scroll='2m',
        search_type='scan',
        size=100000,
        body={
            "query": {
                "match_all": {}
            }
        },
        fields=[])
    new_assets = True
    while new_assets:
        new_assets = False
        page = es.scroll(scroll_id=page['_scroll_id'], scroll='2m')
        for asset in page['hits']['hits']:
            asset['_id'] = str(asset['_id'])
            new_assets = True
            assets = add_entry(assets, mapping[asset['_type']], asset['_id'])
    return assets


def pull_information(workspace, assets_a, assets_b):
    '''
    pull information for the csv
    '''
    result = [['Workspace', 'Asset Type', 'Asset ID', 'Grain']]
    for asset_type in assets_a.keys():
        exclusives = assets_a[asset_type] - assets_b[asset_type]
        result += [[workspace, human_mapping[asset_type],
                    asset_id, grainmapping[asset_id]]
                   for asset_id in exclusives]
                   #add rows to csv (see result definition for headers)
    return result


def compare_es_db(workspace):
    '''
    compare the assets in the elastic search to the assets in the database
    find a not in b, b not in a
    '''
    db_assets = get_db_assets(workspace)
    es_assets = get_es_assets(workspace)
    writetocsv('in_database_but_not_elasticsearch.csv',
               pull_information(workspace, db_assets, es_assets))
    writetocsv('in_elasticsearch_but_not_database.csv',
               pull_information(workspace, es_assets, db_assets))

