import json
from common import maxlen, padding, human_mapping

def find_errors(datum):
    '''
    test the datum
    dictionary keys are the same as in the riskiq database
    A - Host
    B - IP Block
    C - Web Site
    D - Contact
    E - ASN
    F - Domain
    G - SSL Cert
    H - Name Server
    I - Mail Server
    '''
    missing_data = []
    for i in 'ABCDEFGHI':
        if i not in datum:
            missing_data += [human_mapping[i]]
    if missing_data:
        return "missing data: " + ', '.join(missing_data)
    if datum['E'] > datum['B']:
        return "ASNs({}) > IP Blocks({})".format(datum['E'], datum['B'])
    if datum['B'] > datum['A']:
        return "IP Blocks({}) > Hosts({})".format(datum['B'], datum['A'])
    if datum['A'] > datum['C']:
        return "Hosts({}) > Websites({})".format(datum['A'], datum['C'])
    if datum['F'] > datum['C']:
        return "Domains({}) > Websites({})".format(datum['F'], datum['C'])
    if datum['G'] > 10*datum['A']:
        return "SSL Certs ~= Hosts"
    if datum['D'] == 0:
        return "Contacts != 0"

def run_tests():
    '''send each datum to testing to look for anomalies'''
    data = json.load(open('assets.json'))

    result = [(data[workspace]['name'], find_errors(data[workspace]))
              #create a list of tuples (workspace name, errors)
              #for printing out in a minute
              for workspace in data.keys()]

    result = [(workspace, error) for workspace, error in result
              if error is not None]#filter out the none's

    spacing = maxlen(result)
    result = '\n'.join([workspace+padding(spacing, workspace)+ " | " + error
            #workspace name + enough spaces to line everything up + the error
            #separated by newlines
                       for workspace, error in result])
    return result

