import MySQLdb

def maxlen(dataset):
    '''helper function to find the max length in a list of strings'''
    return len(max(dataset, key=lambda x: len(x[0]))[0])+1


def padding(spacing, length):
    '''
    helper function to pad with spaces
    '''
    return ' '*(spacing-len(length))

def querydb(base, args=False):
    '''sends query to riq db'''
    database = MySQLdb.connect(read_default_file="~/.my.cnf",
                               db='linkco_prod')
    cursor = database.cursor()
    if args:
        if not isinstance(args, tuple):
            args = (args, )
        cursor.execute(base, args)
    else:
        cursor.execute(base)
    return [i for i in cursor]

mapping = {
    'HostAsset2':'A',
    'IpBlockAsset':'B',
    'WebSiteAsset':'C',
    'ContactAsset':'D',
    'AsnAsset':'E',
    'DomainAsset':'F',
    'SslCertAsset':'G',
    'NameServerAsset':'H',
    'MailServerAsset':'I',
}

human_mapping = {
    'A':'Host',
    'B':'Ip Block',
    'C':'Web Site',
    'D':'Contact',
    'E':'Asn',
    'F':'Domain',
    'G':'Ssl Cert',
    'H':'Name Server',
    'I':'Mail Server',
}


